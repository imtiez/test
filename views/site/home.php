<?php
use yii\helpers\html;
?>
<div class="site-index">
<?php if (yii::$app->session->hasFlash('message')):?>
    <div class="alert alert-dismissable alert-success">
        <button type="button" class="close" data-dismiss="alert">&times</button>
        <?php echo yii::$app->session->getFlash('message');?>
    </div>
<?php endif;?>
    <div class="jumbotron">
        <h1>Test</h1>
    </div>
    <div class="row">
        <span style="margin-bottom: 30px;"><?=html::a('Create',['/site/create'],['class'=>'btn btn-primary'])?></span>
    </div>

    <div class="body-content">
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">TITEL</th>
                <th scope="col">DESC</th>
                <th scope="col">CATG</th>
                <th scope="col">ACTION</th>
            </tr>
            </thead>
            <tbody>
            <?php if (count($posts)>0): ?>
                <?php foreach ($posts as $post): ?>
                    <tr class="table-active">
                        <th scope="row"><?php echo $post->id;?></th>
                        <td><?php echo $post->titel;?></td>
                        <td><?php echo $post->description;?></td>
                        <td><?php echo $post->category;?></td>
                        <td>
                            <span><?= html::a('View',['view','id' => $post->id],['class'=>'label label-primary'])?></span>
                            <span><?= html::a('Update',['update','id' => $post->id],['class'=>'label label-success'])?></span>
                            <span><?= html::a('Delete',['delete','id' => $post->id],['class'=>'label label-danger'])?></span>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td>no one</td>
                </tr>
            <?php endif;?>
            </tbody>
        </table>

    </div>
</div>
