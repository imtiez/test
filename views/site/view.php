<?php
use yii\helpers\html;
use yii\widgets\ActiveForm;
?>
<div class="site-index">

    <h1>View</h1>
    <div class="body-content">
        <ul class="list-group">
            <li class="list-group-item d-flex justify-content-between align-items-center">
                <?php echo $post->titel; ?>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                <?php echo $post->description; ?>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                <?php echo $post->category; ?>
            </li>
        </ul>
        <div class="row">
            <a href=<?php echo yii::$app->homeUrl;?> class = "btn btn-primary">Back</a>
        </div>
    </div>
</div>
