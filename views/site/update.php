<?php
use yii\helpers\html;
use yii\widgets\ActiveForm;
?>
<div class="site-index">

    <h1>Update</h1>


    <div class="body-content">
        <?php $form = ActiveForm::begin()?>
        <div class="row">
            <div class="form-group">
                <div class="col-lg-6">
                    <?= $form->field($post,'titel');?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-lg-6">
                    <?= $form->field($post,'description')->textarea(['rows'=>'6']);?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-lg-6">
                    <?php $items=['e-commerce'=>'e-commerce','CMS','MVC'=>'MVC',];?>
                    <?= $form->field($post,'category')->dropDownList($items,['prompt'=>'Select']);?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-lg-6">
                    <div class="col-lg-3">
                        <?= Html::submitButton('Update post',['class'=>'btn btn-primary']);?>
                    </div>
                    <div class="col-lg-2">
                        <a href=<?php echo yii::$app->homeUrl;?> class = "btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end()?>
    </div>
</div>
